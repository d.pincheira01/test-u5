package cl.ufro.dci.testu5;

import model.Network;
import cl.ufro.dci.testu5.controllers.Wine;

public class App {

    public static void main(String[] args) {
        Wine wineTest = new Wine();
        Network<Integer>.Results results = wineTest.classify();
        System.out.println(results.correct + " correct of " + results.trials + " = " + results.percentage * 100 + "%");
    }
}
